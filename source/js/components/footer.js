

$(document).ready(function () {


	$(".footer .navigation__item_mob").on("click", function() {
		$(".footer .navigation__mobile .header__deep").hide('slow');
		$(".footer .navigation__item_dropdown_mobile").removeClass("navigation__item_dropdown_mobile_active");
		$(".footer .navigation__item_arrow").removeClass("navigation__item_arrow_active");

		$( ".footer .navigation__mobile" ).toggle( "slow" );
		$(this).toggleClass("navigation__item_mob_active");
	} ); 
	$(".footer .navigation__item_arrow").on("click", function() {
		console.log(this);
		$(".footer .navigation__mobile .header__deep").hide('slow');
		$(".footer .navigation__item_arrow").removeClass("navigation__item_arrow_active");

		if( $(this).parent().parent().hasClass("navigation__item_dropdown_mobile_active") ) {
			$(this).parent().parent().removeClass("navigation__item_dropdown_mobile_active");
			$(this).removeClass("navigation__item_arrow_active");
		} else {
			$(this).parent().parent().find(".header__deep").toggle('slow');
			$(this).parent().parent().addClass("navigation__item_dropdown_mobile_active");
			$(this).addClass("navigation__item_arrow_active");
		}
		
	});


	$(window).resize(function() {
		if( $(window).width() > 991 ) {
			$(".footer .navigation__item_mob").removeClass("navigation__item_mob_active");  
			$(".footer .navigation__mobile .header__deep").hide('slow');
			$(".footer .navigation__mobile").hide( "slow" );
			$(".footer .navigation__item_dropdown_mobile").removeClass("navigation__item_dropdown_mobile_active");
			$(".footer .navigation__item_arrow").removeClass("navigation__item_arrow_active");
		}
	});
})


