$(document).ready(function () {
	$(".slider_welcome").each(function(index, element) {
		$(element).slick({
			lazyLoad: 'ondemand',
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 300,
			infinite: false,
			dots: true,
			arrows: false,
			fade: true,
			cssEase: 'linear',
			 responsive: [
				{
				breakpoint: 991,
				settings: {
						autoplay: true,
						autoplaySpeed: 2000,
						infinite: true,
						speed: 300,
						fade: false,
						dots: false,
					}
				}
    		]
		});
	});





	$(".slider_news").each(function(index, element) {
		$(element).slick({
			lazyLoad: 'ondemand',
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 300,
			infinite: false,
			dots: true,
			arrows: true,
			nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#E3E3E3" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#E3E3E3" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			afterChange: function() {
			}
		});
	});
	$(".slider_news").on('beforeChange', function(event, slick, currentSlide, nextSlide){
	        $(".slider-news__content").css("display", "none" );
	});

	$(".slider_avto").each(function(index, element) {
		$(element).slick({
			lazyLoad: 'ondemand',
			slidesToShow: 4,
			slidesToScroll: 1,
			speed: 300,
			infinite: false,
			dots: false,
			arrows: true,
			nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#E3E3E3" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#E3E3E3" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			responsive: [
				{
				breakpoint: 991,
				settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				}
    		]
		});
	});



	



	$(".slider_preview").each(function(index, element) {
		$(element).slick({
			lazyLoad: 'ondemand',
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 300,
			infinite: false,
			dots: true,
			arrows: false,
			fade: true,
			cssEase: 'linear',
			responsive: [
				{
				breakpoint: 991,
				settings: {
						infinite: true,
						speed: 300,
						fade: false,
					}
				}
    		]
		});
	});
})


