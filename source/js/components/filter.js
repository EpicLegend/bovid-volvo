$(document).ready(function () {
	$("#model").selectmenu();
	$( "#price" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 75, 300 ],
		slide: function( event, ui ) {
			$( ".price__out .price__prev .filter__text" ).html( "От " + ui.values[ 0 ] );
			$( ".price__out .price__next .filter__text" ).html( "До " + ui.values[ 1 ] );
		}
	});
	$( "#year" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 75, 300 ],
		slide: function( event, ui ) {
			$( ".year__out .price__prev .filter__text" ).html( "От " + ui.values[ 0 ] );
			$( ".year__out .price__next .filter__text" ).html( "До " + ui.values[ 1 ] );
		}
	});
	$( "#run" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 75, 300 ],
		slide: function( event, ui ) {
			$( ".run__out .price__prev .filter__text" ).html( "От " + ui.values[ 0 ] );
			$( ".run__out .price__next .filter__text" ).html( "До " + ui.values[ 1 ] );
		}
	});
	$("#typeengine").selectmenu();
	$("#typekpp").selectmenu();
	$("#privod").selectmenu();
	$("#volume").selectmenu();

	$("#form_1").selectmenu();
	$("#form_2").selectmenu();

	$("#form_3").selectmenu();
	$("#form_4").selectmenu();
	$("#form_5").selectmenu();
})


