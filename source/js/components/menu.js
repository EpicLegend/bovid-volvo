

$(document).ready(function () {
	$(".header__second .navigation__item_mob").on("click", function() {
		$(".header__second .navigation__mobile .header__deep").hide('slow');
		$(".header__second .navigation__item_dropdown_mobile").removeClass("navigation__item_dropdown_mobile_active");
		$(".header__second .navigation__item_arrow").removeClass("navigation__item_arrow_active");

		$( ".header__second .navigation__mobile" ).toggle( "slow" );
		$(this).toggleClass("navigation__item_mob_active");
	} ); 

	$(".header__second .navigation__item_arrow").on("click", function() {

		$(".header__second .navigation__mobile .header__deep").hide('slow');
		$(".header__second .navigation__item_arrow").removeClass("navigation__item_arrow_active");

		if( $(this).parent().parent().hasClass("navigation__item_dropdown_mobile_active") ) {
			$(this).parent().parent().removeClass("navigation__item_dropdown_mobile_active");
			$(this).removeClass("navigation__item_arrow_active");
		} else {
			$(this).parent().parent().find(".header__deep").toggle('slow');
			$(this).parent().parent().addClass("navigation__item_dropdown_mobile_active");
			$(this).addClass("navigation__item_arrow_active");
		}
		
	});


	$(window).resize(function() {
		console.log("wind");
		if( $(window).width() > 991 ) {
			$(".header__second .navigation__item_mob").removeClass("navigation__item_mob_active");  
			$(".header__second .navigation__mobile .header__deep").hide('slow');
			$(".header__second .navigation__mobile").hide( "slow" );
			$(".header__second .navigation__item_dropdown_mobile").removeClass("navigation__item_dropdown_mobile_active");
			$(".header__second .navigation__item_arrow").removeClass("navigation__item_arrow_active");
		}
	});
})


