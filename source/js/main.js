'use strict';
//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
//= library/slick.js
//= library/jquery-ui.js

//= components/menu.js
//= components/sliderSlick.js
//= components/footer.js
//= components/carrunhover.js
//= components/filter.js
//= components/carItem.js


$(document).ready(function () {


	/*
	// 
	$.post( "test.php", $( "form.calc" ).serialize(), function (data) {
		// ответ от сервера
	}, "json" );
	*/





	/* START Открытие меню */
	$(".btn__menu").on("click", function () {
		$(this).toggleClass("active");

		if ( $(this).hasClass("active") ) {
			$(".navigation__content").addClass("active");
		} else {
			$(".navigation__content").removeClass("active");
		}
	});

	$(".btn_header").on("click", function () {
		if( $(".navigation__content").hasClass("active") ) {
			$(".navigation__content").removeClass("active");
			$(".btn__menu").removeClass("active");
			$("#burger").removeClass("active");
		}
	});
	/* END откртие меню*/

	if ( $(window).scrollTop() > 0 ) {
        //$("header").addClass("bg-dark");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			//$("header").addClass("bg-dark");
		}  else {
			//$("header").removeClass("bg-dark");
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn-to-top").addClass("btn-to-top_active");
		} else {
			$(".btn-to-top").removeClass("btn-to-top_active");
		}

	});
	$('.btn-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
	/* END кнопка вверх */



	// для слайдера новостей на главной
	$(".slider-news__content").css("width", $(".slider-news__item_width").width() );
	$(window).resize(function () {
		if ( $(window).width() < 991 ) {
			$(".slider-news__content").css("width", "100%" );
			$(".slider-news__content").css("display", "block" );
		} else {
			$(".slider-news__content").css("width", $(".slider-news__item_width").width() );
		}
		
	});
});


